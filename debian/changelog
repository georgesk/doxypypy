doxypypy (0.8.8.7-3) unstable; urgency=medium

  * dropped the dependency on python3-pkg-resources, which became
    useless since version 0.8.8.7. Closes: #1083373

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 05 Oct 2024 16:10:00 +0200

doxypypy (0.8.8.7-2) unstable; urgency=medium

  * upload to unstable

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 11 Jun 2023 11:40:33 +0200

doxypypy (0.8.8.7-1) experimental; urgency=medium

  * New upstream version 0.8.8.6.1
  * New upstream version 0.8.8.7
  * Bump Standards-Version: 4.6.2
  * refreshed debian patches

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 01 May 2023 17:28:25 +0200

doxypypy (0.8.8.6-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Add debian/watch file, using pypi.
  * Update standards version to 4.6.1, no changes needed.

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 21 Feb 2023 18:04:38 +0100

doxypypy (0.8.8.6-4) unstable; urgency=medium

  * added a dependency on python3-chardet
  * ugraded standards: Standards-Version: 4.5.0, debhelper-compat (= 13)

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 14 Dec 2020 16:06:49 +0100

doxypypy (0.8.8.6-3) unstable; urgency=medium

  * patched doxypypy.py to avoid the following bug:
      NodeVisitor:visit_Constant() argument containingNodes unexpected
      keyword (python 3.8)
    thanks to svigerske@github.com, see
    https://github.com/Feneric/doxypypy/issues/70

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 19 Jul 2020 18:33:00 +0200

doxypypy (0.8.8.6-2) unstable; urgency=medium

  * second release, required by policy: source uploads are mandatory,
    while NEW packages enter the queue as binary packages.
  * fixed the URLs for Git.

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 01 Feb 2020 20:12:36 +0100

doxypypy (0.8.8.6-1) unstable; urgency=medium

  * Initial release (Closes: #945952)

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 01 Dec 2019 17:11:22 +0100
